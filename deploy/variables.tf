variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "email@example.com"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS Postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "112034238486.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for Proxy"
  default     = "112034238486.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"

}

variable "django_secret_key" {
  description = "Secret Key for Django App"
}

variable "dns_zone_name" {
  description = "Domain Name"
  default     = "cloudserviceschile.net"
}

variable "subdomain" {
  description = "SubDomain per Environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}
